const express = require('express');
const logger = require("pino")();
const accessLog = require("pino-http")({
    useLevel: "info"
});
const app = express();

app.use(accessLog);
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  logger.info(`Example app listening at http://localhost:${port}`)
})